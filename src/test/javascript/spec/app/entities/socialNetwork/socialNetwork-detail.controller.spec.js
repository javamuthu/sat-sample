'use strict';

describe('SocialNetwork Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockSocialNetwork;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockSocialNetwork = jasmine.createSpy('MockSocialNetwork');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'SocialNetwork': MockSocialNetwork
        };
        createController = function() {
            $injector.get('$controller')("SocialNetworkDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'satApp:socialNetworkUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
