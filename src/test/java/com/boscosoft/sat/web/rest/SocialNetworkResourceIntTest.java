package com.boscosoft.sat.web.rest;

import com.boscosoft.sat.Application;
import com.boscosoft.sat.domain.SocialNetwork;
import com.boscosoft.sat.repository.SocialNetworkRepository;
import com.boscosoft.sat.service.SocialNetworkService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SocialNetworkResource REST controller.
 *
 * @see SocialNetworkResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SocialNetworkResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    @Inject
    private SocialNetworkRepository socialNetworkRepository;

    @Inject
    private SocialNetworkService socialNetworkService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSocialNetworkMockMvc;

    private SocialNetwork socialNetwork;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SocialNetworkResource socialNetworkResource = new SocialNetworkResource();
        ReflectionTestUtils.setField(socialNetworkResource, "socialNetworkService", socialNetworkService);
        this.restSocialNetworkMockMvc = MockMvcBuilders.standaloneSetup(socialNetworkResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        socialNetworkRepository.deleteAll();
        socialNetwork = new SocialNetwork();
        socialNetwork.setName(DEFAULT_NAME);
        socialNetwork.setDescription(DEFAULT_DESCRIPTION);
        socialNetwork.setIsActive(DEFAULT_IS_ACTIVE);
    }

    @Test
    public void createSocialNetwork() throws Exception {
        int databaseSizeBeforeCreate = socialNetworkRepository.findAll().size();

        // Create the SocialNetwork

        restSocialNetworkMockMvc.perform(post("/api/socialNetworks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(socialNetwork)))
                .andExpect(status().isCreated());

        // Validate the SocialNetwork in the database
        List<SocialNetwork> socialNetworks = socialNetworkRepository.findAll();
        assertThat(socialNetworks).hasSize(databaseSizeBeforeCreate + 1);
        SocialNetwork testSocialNetwork = socialNetworks.get(socialNetworks.size() - 1);
        assertThat(testSocialNetwork.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSocialNetwork.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSocialNetwork.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
    }

    @Test
    public void getAllSocialNetworks() throws Exception {
        // Initialize the database
        socialNetworkRepository.save(socialNetwork);

        // Get all the socialNetworks
        restSocialNetworkMockMvc.perform(get("/api/socialNetworks?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(socialNetwork.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())));
    }

    @Test
    public void getSocialNetwork() throws Exception {
        // Initialize the database
        socialNetworkRepository.save(socialNetwork);

        // Get the socialNetwork
        restSocialNetworkMockMvc.perform(get("/api/socialNetworks/{id}", socialNetwork.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(socialNetwork.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()));
    }

    @Test
    public void getNonExistingSocialNetwork() throws Exception {
        // Get the socialNetwork
        restSocialNetworkMockMvc.perform(get("/api/socialNetworks/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateSocialNetwork() throws Exception {
        // Initialize the database
        socialNetworkRepository.save(socialNetwork);

		int databaseSizeBeforeUpdate = socialNetworkRepository.findAll().size();

        // Update the socialNetwork
        socialNetwork.setName(UPDATED_NAME);
        socialNetwork.setDescription(UPDATED_DESCRIPTION);
        socialNetwork.setIsActive(UPDATED_IS_ACTIVE);

        restSocialNetworkMockMvc.perform(put("/api/socialNetworks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(socialNetwork)))
                .andExpect(status().isOk());

        // Validate the SocialNetwork in the database
        List<SocialNetwork> socialNetworks = socialNetworkRepository.findAll();
        assertThat(socialNetworks).hasSize(databaseSizeBeforeUpdate);
        SocialNetwork testSocialNetwork = socialNetworks.get(socialNetworks.size() - 1);
        assertThat(testSocialNetwork.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSocialNetwork.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSocialNetwork.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
    }

    @Test
    public void deleteSocialNetwork() throws Exception {
        // Initialize the database
        socialNetworkRepository.save(socialNetwork);

		int databaseSizeBeforeDelete = socialNetworkRepository.findAll().size();

        // Get the socialNetwork
        restSocialNetworkMockMvc.perform(delete("/api/socialNetworks/{id}", socialNetwork.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SocialNetwork> socialNetworks = socialNetworkRepository.findAll();
        assertThat(socialNetworks).hasSize(databaseSizeBeforeDelete - 1);
    }
}
