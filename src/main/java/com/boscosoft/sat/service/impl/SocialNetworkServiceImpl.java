package com.boscosoft.sat.service.impl;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.boscosoft.sat.domain.SocialNetwork;
import com.boscosoft.sat.repository.SocialNetworkRepository;
import com.boscosoft.sat.service.SocialNetworkService;

/**
 * Service Implementation for managing SocialNetwork.
 */
@Service
public class SocialNetworkServiceImpl implements SocialNetworkService{

    private final Logger log = LoggerFactory.getLogger(SocialNetworkServiceImpl.class);
    
    @Inject
    private SocialNetworkRepository socialNetworkRepository;
    
    /**
     * Save a socialNetwork.
     * @return the persisted entity
     */
    public SocialNetwork save(SocialNetwork socialNetwork) {
        //log.debug("Request to save SocialNetwork : {}", socialNetwork);
        SocialNetwork result = socialNetworkRepository.save(socialNetwork);
        return result;
    }

    /**
     *  get all the socialNetworks.
     *  @return the list of entities
     */
    public Page<SocialNetwork> findAll(Pageable pageable) {
       // log.debug("Request to get all SocialNetworks");
        Page<SocialNetwork> result = socialNetworkRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one socialNetwork by id.
     *  @return the entity
     */
    public SocialNetwork findOne(String id) {
       // log.debug("Request to get SocialNetwork : {}", id);
        SocialNetwork socialNetwork = socialNetworkRepository.findOne(id);
        return socialNetwork;
    }

    /**
     *  delete the  socialNetwork by id.
     */
    public void delete(String id) {
      //  log.debug("Request to delete SocialNetwork : {}", id);
        socialNetworkRepository.delete(id);
    }
}
