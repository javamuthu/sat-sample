/**
 * 
 */
package com.boscosoft.sat.service.impl;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.boscosoft.sat.domain.Authority;
import com.boscosoft.sat.repository.AuthorityRepository;
import com.boscosoft.sat.service.AuthorityService;

/**
 * @author muthu
 *
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {
	
    @Inject
    private AuthorityRepository authorityRepository;

	/* (non-Javadoc)
	 * @see com.boscosoft.sat.service.RoleService#save(com.boscosoft.sat.domain.Authority)
	 */
	@Override
	public Authority save(Authority authority) {
		// TODO Auto-generated method stub
		Authority result = authorityRepository.save(authority);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.boscosoft.sat.service.RoleService#findAll(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Authority> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		Page<Authority> result = authorityRepository.findAll(pageable);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.boscosoft.sat.service.RoleService#findOne(java.lang.String)
	 */
	@Override
	public Authority findOne(String name) {
		// TODO Auto-generated method stub
		Authority result = authorityRepository.findOne(name);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.boscosoft.sat.service.RoleService#delete(java.lang.String)
	 */
	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		authorityRepository.delete(name);
	}

}
