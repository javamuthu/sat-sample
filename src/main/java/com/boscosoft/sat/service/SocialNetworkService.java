package com.boscosoft.sat.service;

import com.boscosoft.sat.domain.SocialNetwork;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing SocialNetwork.
 */
public interface SocialNetworkService {

    /**
     * Save a socialNetwork.
     * @return the persisted entity
     */
    public SocialNetwork save(SocialNetwork socialNetwork);

    /**
     *  get all the socialNetworks.
     *  @return the list of entities
     */
    public Page<SocialNetwork> findAll(Pageable pageable);

    /**
     *  get the "id" socialNetwork.
     *  @return the entity
     */
    public SocialNetwork findOne(String id);

    /**
     *  delete the "id" socialNetwork.
     */
    public void delete(String id);
}
