package com.boscosoft.sat.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.boscosoft.sat.domain.Authority;
import com.boscosoft.sat.domain.SocialNetwork;


/**
 * 
 * @author muthu
 *
 */

/**
 * Service Interface for managing Role management.
 */
public interface AuthorityService {
	
	/**
     * Save a authority.
     * @return the persisted entity
     */
    public Authority save(Authority authority);
    
    /**
     *  get all the authority.
     *  @return the list of entities
     */
    public Page<Authority> findAll(Pageable pageable);

    /**
     *  get the "id" socialNetwork.
     *  @return the entity
     */
    public Authority findOne(String id);

    /**
     *  delete the "id" socialNetwork.
     */
    public void delete(String id);

}
