package com.boscosoft.sat.repository;

import com.boscosoft.sat.domain.SocialNetwork;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the SocialNetwork entity.
 */
public interface SocialNetworkRepository extends MongoRepository<SocialNetwork,String> {

}
