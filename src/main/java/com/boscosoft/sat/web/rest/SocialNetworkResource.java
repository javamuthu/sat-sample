package com.boscosoft.sat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.boscosoft.sat.domain.SocialNetwork;
import com.boscosoft.sat.service.SocialNetworkService;
import com.boscosoft.sat.web.rest.util.HeaderUtil;
import com.boscosoft.sat.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SocialNetwork.
 */
@RestController
@RequestMapping("/api")
public class SocialNetworkResource {

   // private final Logger log = LoggerFactory.getLogger(SocialNetworkResource.class);
        
    @Inject
    private SocialNetworkService socialNetworkService;
    
    /**
     * POST  /socialNetworks -> Create a new socialNetwork.
     */
    @RequestMapping(value = "/socialNetworks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SocialNetwork> createSocialNetwork(@RequestBody SocialNetwork socialNetwork) throws URISyntaxException {
       // log.debug("REST request to save SocialNetwork : {}", socialNetwork);
        if (socialNetwork.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("socialNetwork", "idexists", "A new socialNetwork cannot already have an ID")).body(null);
        }
        SocialNetwork result = socialNetworkService.save(socialNetwork);
        return ResponseEntity.created(new URI("/api/socialNetworks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("socialNetwork", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /socialNetworks -> Updates an existing socialNetwork.
     */
    @RequestMapping(value = "/socialNetworks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SocialNetwork> updateSocialNetwork(@RequestBody SocialNetwork socialNetwork) throws URISyntaxException {
     //   log.debug("REST request to update SocialNetwork : {}", socialNetwork);
        if (socialNetwork.getId() == null) {
            return createSocialNetwork(socialNetwork);
        }
        SocialNetwork result = socialNetworkService.save(socialNetwork);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("socialNetwork", socialNetwork.getId().toString()))
            .body(result);
    }

    /**
     * GET  /socialNetworks -> get all the socialNetworks.
     */
    @RequestMapping(value = "/socialNetworks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SocialNetwork>> getAllSocialNetworks(Pageable pageable)
        throws URISyntaxException {
      //  log.debug("REST request to get a page of SocialNetworks");
        Page<SocialNetwork> page = socialNetworkService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/socialNetworks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /socialNetworks/:id -> get the "id" socialNetwork.
     */
    @RequestMapping(value = "/socialNetworks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SocialNetwork> getSocialNetwork(@PathVariable String id) {
       // log.debug("REST request to get SocialNetwork : {}", id);
        SocialNetwork socialNetwork = socialNetworkService.findOne(id);
        return Optional.ofNullable(socialNetwork)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /socialNetworks/:id -> delete the "id" socialNetwork.
     */
    @RequestMapping(value = "/socialNetworks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSocialNetwork(@PathVariable String id) {
       // log.debug("REST request to delete SocialNetwork : {}", id);
        socialNetworkService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("socialNetwork", id.toString())).build();
    }
}
