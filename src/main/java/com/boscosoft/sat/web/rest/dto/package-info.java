/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.boscosoft.sat.web.rest.dto;
