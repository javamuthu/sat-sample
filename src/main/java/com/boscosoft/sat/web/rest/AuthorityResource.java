/**
 * 
 */
package com.boscosoft.sat.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.boscosoft.sat.domain.Authority;
import com.boscosoft.sat.service.AuthorityService;
import com.boscosoft.sat.web.rest.util.HeaderUtil;
import com.boscosoft.sat.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

/**
 * @author muthu
 *
 */
/**
 * REST controller for managing SocialNetwork.
 */
@RestController
@RequestMapping("/api")
public class AuthorityResource {
	
	@Inject
	private AuthorityService authorityService;
	
	 /**
     * POST  /authority -> Create a new authority.
     */
    @RequestMapping(value = "/authority",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Authority> createAuthority(@RequestBody Authority authority) throws URISyntaxException {
        if (authority.getName() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("authority", "idexists", "A new authority cannot already have an ID")).body(null);
        }
        Authority result = authorityService.save(authority);
        return ResponseEntity.created(new URI("/api/authority/" + result.getName()))
            .headers(HeaderUtil.createEntityCreationAlert("authority", result.getName().toString()))
            .body(result);
    }

    /**
     * PUT  /authority -> Updates an existing authority.
     */
    @RequestMapping(value = "/authority",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Authority> updateAuthority(@RequestBody Authority authority) throws URISyntaxException {
        if (authority.getName() == null) {
            return createAuthority(authority);
        }
        Authority result = authorityService.save(authority);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("authority", authority.getName().toString()))
            .body(result);
    }
    
    /**
     * GET  /authority -> get all the authority.
     */
    @RequestMapping(value = "/authority",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Authority>> getAllAuthorities(Pageable pageable)
        throws URISyntaxException {
        Page<Authority> page = authorityService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/authority");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /authority/:name -> get the "name" authority.
     */
    @RequestMapping(value = "/authority/{name}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Authority> getAuthority(@PathVariable String name) {
        Authority authority = authorityService.findOne(name);
        return Optional.ofNullable(authority)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /authority/:name -> delete the "name" authority.
     */
    @RequestMapping(value = "/authority/{name}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAuthority(@PathVariable String name) {
        authorityService.delete(name);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("authority", name.toString())).build();
    }


}
