'use strict';

angular.module('satApp')
    .config(function (uibPagerConfig) {
        uibPagerConfig.itemsPerPage = 10;
        uibPagerConfig.previousText = '«';
        uibPagerConfig.nextText = '»';
    });
