'use strict';

angular.module('satApp')
    .factory('Authority', function ($resource, DateUtils) {
        return $resource('api/authority/:name', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
