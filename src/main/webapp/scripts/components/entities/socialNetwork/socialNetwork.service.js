'use strict';

angular.module('satApp')
    .factory('SocialNetwork', function ($resource, DateUtils) {
        return $resource('api/socialNetworks/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
