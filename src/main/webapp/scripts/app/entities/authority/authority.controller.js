'use strict';

angular.module('satApp')
    .controller('AuthorityController', function ($scope, $state, Authority, ParseLinks) {

        $scope.authorities = [];
        $scope.predicate = 'name';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
        	Authority.query({page: $scope.page - 1, size: 10, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'name']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.authorities = result;
                console.log($scope.authorities);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.authority = {
                name: null,
                description: null,
            };
        };
        
        /**
         * Added by Muthu Kumar M
         */
        
        $scope.checkAll = function () {
            if ($scope.selectedAll) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }
            angular.forEach($scope.authorities, function (authority) {
            	authority.Selected = $scope.selectedAll;
            });

        };
        
        
    });
