'use strict';

angular.module('satApp').controller('AuthorityDialogController',
    ['$scope', '$stateParams', '$window', 'entity', 'Authority',
        function($scope, $stateParams, $window, entity, Authority) {
    	
        $scope.authority = entity;
        $scope.load = function(name) {
        	Authority.get({name : name}, function(result) {
                $scope.authority = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.isSaving = false;
            $window.location.href="#/authorities";
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.authority.name != null) {
            	Authority.update($scope.authority, onSaveSuccess, onSaveError);
            } else {
            	Authority.save($scope.authority, onSaveSuccess, onSaveError);
            }
        };

}]);
