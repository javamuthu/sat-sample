'use strict';

angular.module('satApp')
    .controller('AuthorityDetailController', function ($scope, $rootScope, $stateParams, entity, Authority) {
        $scope.authority = entity;
        $scope.load = function (id) {
        	Authority.get({id: id}, function(result) {
                $scope.authority = result;
            });
        };
        var unsubscribe = $rootScope.$on('satApp:authorityUpdate', function(event, result) {
            $scope.socialNetwork = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
