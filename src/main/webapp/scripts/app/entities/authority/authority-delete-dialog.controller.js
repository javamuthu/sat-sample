'use strict';

angular.module('satApp')
	.controller('AuthorityDeleteController', function($scope, $uibModalInstance, entity, Authority) {

        $scope.authority = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (name) {
        	Authority.delete({name: name},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
