'use strict';

angular.module('satApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('authority', {
                parent: 'entity',
                url: '/authorities',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'Role Management'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/authority/authority.html',
                        controller: 'AuthorityController'
                    }
                },
                resolve: {
                }
            }).state('authority.detail', {
                parent: 'entity',
                url: '/authority/{name}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'Role Management'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/authority/authority-detail.html',
                        controller: 'AuthorityDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Authority', function($stateParams, Authority) {
                        return Authority.get({name : $stateParams.name});
                    }]
                }
            }).state('authority.new', {
                parent: 'authority',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'Role Management'
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/authority/authority-dialog.html',
                         controller: 'AuthorityDialogController',
                    }
                },
                resolve: {
                	 entity: function () {
                         return {
                             name: null,
                             description: null,
                         };
                     }
                }
            }).state('authority.edit', {
                parent: 'authority',
                url: '/{name}/edit',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/authority/authority-dialog.html',
                         controller: 'AuthorityDialogController',
                    }
                },
                resolve: {
                	  entity: ['Authority','$stateParams', function(Authority, $stateParams) {
                          return Authority.get({name : $stateParams.name});
                      }]
                }
            }).state('authority.delete', {
                parent: 'authority',
                url: '/{name}/delete',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/authority/authority-delete-dialog.html',
                        controller: 'AuthorityDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Authority', function(Authority) {
                                return Authority.get({name : $stateParams.name});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('authority', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
