'use strict';

angular.module('satApp')
    .controller('SocialNetworkController', function ($scope, $state, SocialNetwork, ParseLinks) {

        $scope.socialNetworks = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            SocialNetwork.query({page: $scope.page - 1, size: 10, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.socialNetworks = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.socialNetwork = {
                name: null,
                description: null,
                isActive: null,
                id: null
            };
        };
        
        /**
         * Added by Muthu Kumar M
         */
        
        $scope.checkAll = function () {
            if ($scope.selectedAll) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }
            angular.forEach($scope.socialNetworks, function (socialNetwork) {
            	socialNetwork.Selected = $scope.selectedAll;
            });

        };
        
        
    });
