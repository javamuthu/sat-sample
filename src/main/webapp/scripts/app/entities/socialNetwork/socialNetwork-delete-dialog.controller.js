'use strict';

angular.module('satApp')
	.controller('SocialNetworkDeleteController', function($scope, $uibModalInstance, entity, SocialNetwork) {

        $scope.socialNetwork = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            SocialNetwork.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
