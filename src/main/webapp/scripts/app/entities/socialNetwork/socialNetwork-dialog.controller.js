'use strict';

angular.module('satApp').controller('SocialNetworkDialogController',
    ['$scope', '$stateParams', '$window', 'entity', 'SocialNetwork',
        function($scope, $stateParams, $window, entity, SocialNetwork) {
    	
        $scope.socialNetwork = entity;
        $scope.load = function(id) {
            SocialNetwork.get({id : id}, function(result) {
                $scope.socialNetwork = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.isSaving = false;
            $window.location.href="#/socialNetworks";
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.socialNetwork.id != null) {
                SocialNetwork.update($scope.socialNetwork, onSaveSuccess, onSaveError);
            } else {
                SocialNetwork.save($scope.socialNetwork, onSaveSuccess, onSaveError);
            }
        };

}]);
