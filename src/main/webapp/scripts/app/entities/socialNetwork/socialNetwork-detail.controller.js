'use strict';

angular.module('satApp')
    .controller('SocialNetworkDetailController', function ($scope, $rootScope, $stateParams, entity, SocialNetwork) {
        $scope.socialNetwork = entity;
        $scope.load = function (id) {
            SocialNetwork.get({id: id}, function(result) {
                $scope.socialNetwork = result;
            });
        };
        var unsubscribe = $rootScope.$on('satApp:socialNetworkUpdate', function(event, result) {
            $scope.socialNetwork = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
