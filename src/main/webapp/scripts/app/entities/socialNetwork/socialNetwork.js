'use strict';

angular.module('satApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('socialNetwork', {
                parent: 'entity',
                url: '/socialNetworks',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'SocialNetworks'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/socialNetwork/socialNetworks.html',
                        controller: 'SocialNetworkController'
                    }
                },
                resolve: {
                }
            })
            .state('socialNetwork.detail', {
                parent: 'entity',
                url: '/socialNetwork/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'SocialNetwork'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/socialNetwork/socialNetwork-detail.html',
                        controller: 'SocialNetworkDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'SocialNetwork', function($stateParams, SocialNetwork) {
                        return SocialNetwork.get({id : $stateParams.id});
                    }]
                }
            }).state('socialNetwork.new', {
                parent: 'socialNetwork',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'Social NetWork'
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/socialNetwork/socialNetwork-dialog.html',
                         controller: 'SocialNetworkDialogController',
                    }
                },
                resolve: {
                	 entity: function () {
                         return {
                             name: null,
                             description: null,
                             isActive: null,
                             id: null
                         };
                     }
                }
            }).state('socialNetwork.edit', {
                parent: 'socialNetwork',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/socialNetwork/socialNetwork-dialog.html',
                         controller: 'SocialNetworkDialogController',
                    }
                },
                resolve: {
                	  entity: ['SocialNetwork','$stateParams', function(SocialNetwork, $stateParams) {
                          return SocialNetwork.get({id : $stateParams.id});
                      }]
                }
            })
            .state('socialNetwork.delete', {
                parent: 'socialNetwork',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/socialNetwork/socialNetwork-delete-dialog.html',
                        controller: 'SocialNetworkDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['SocialNetwork', function(SocialNetwork) {
                                return SocialNetwork.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('socialNetwork', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
